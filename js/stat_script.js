var serverIp = "202.181.241.56";
var serverPath = "/"+local+"/api/ird";
var logout_serverPath = "/"+local+"/api";
var audioFile = "media/Ding-small-bell.mp3";
	
var Order = Backbone.Model.extend({
	
	getTotal : function () {
		return this.get('total');
	},
	getCancelled : function () {
		return this.get('cancelled');
	},
	getProcessed : function () {
		return this.get('processed');
	},
	getPlaced1 : function () {
		return this.get('placed1');
	},
	getPlaced2 : function () {
		return this.get('placed2');
	},
	getPlaced3 : function () {
		return this.get('placed3');
	},
	getPlaced4 : function () {
		return this.get('placed4');
	},
	getDeliver1 : function () {
		return this.get('deliver1');
	},
	getDeliver2 : function () {
		return this.get('deliver2');
	},
	getDeliver3 : function () {
		return this.get('deliver3');
	},
	getDeliver4 : function () {
		return this.get('deliver4');
	}
});

var Orders = Backbone.Collection.extend({

	model : Order,
	searchYear : false,
	searchMonth : false,
	searchRoom : false,
	
	url : function () {
		console.log("url");
		//-- test --//
		//return 'http://'+serverIp+serverPath+'/getOrder.php?' + 'date=' + encodeURIComponent(this.searchDate) + '&status=' + encodeURIComponent(this.searchStatus) + '&room=' + encodeURIComponent(this.searchRoom);
		
		return '..'+serverPath+'/getOrderStat.php?' + 'year=' + encodeURIComponent(this.searchYear) + '&month=' + encodeURIComponent(this.searchMonth) + '&room=' + encodeURIComponent(this.searchRoom);
	},
	parse : function (response) {
		console.log("parse: "+response.data);
		return response.data;
	},
	searchFor : function (year, month, room) {
		console.log("searchFor: "+'year=' + encodeURIComponent(year) + '&month=' + encodeURIComponent(month) + '&room=' + encodeURIComponent(room));
		if (year==null || year == 0) year = getTodayYear();
		if (month==null || month==0) month = getTodayMonth();
		if (room==null || room == 0) room="";
		this.searchYear = year;
		this.searchMonth = month;
		this.searchRoom = room;
		this.fetch();
		return this;
	}
});

var OrdersList = Backbone.View.extend({
	tagName: 'ul',
	id: 'results',
	className: 'list-group',
	template: function () { return ''; },
	initialize: function(options) {
		console.log("OrdersList init");
		this.listenTo(this.collection, 'sync', this.render);
		if (options.template) {
			this.template = options.template;
		}
	},
	render: function () {
		console.log("OrdersList render ");
		var templateData = {
			results: this.collection.map(this._generateRowData)
		};
		var html = this.template(templateData);
		this.$el.html(html);
		$(window).scrollReadPos();
		if ($(".poOn").length>0){
			$(".poOn").effect("shake");
			//playSound();
		}
		
		return this;
	},
	_generateRowData: function (model) {
		return {
			total:           		model.getTotal(),
			cancelled:       	model.getCancelled(),
			processed:        model.getProcessed(),
			placed1: 		    model.getPlaced1(),
			placed2: 		    model.getPlaced2(),
			placed3:    		model.getPlaced3(),
			placed4:    		model.getPlaced4(),
			deliver1:   		model.getDeliver1(),
			deliver2:     		model.getDeliver2(),
			deliver3:         	model.getDeliver3(),
			deliver4:      		model.getDeliver4()
		};
	},
	events: {
		
	},
	pendingOrder: function(ev) {
		
	},
	cancel: function() {
		resultView.render();
	}
	
});

var SearchForm = Backbone.View.extend({
	initialize: function () {
		if (this.collection) {
			this.listenTo(this.collection, 'sync', this.render);
		}
	},
	render: function () {
		this.$("#year").val(this.collection.searchYear);
		this.$("#month").val(this.collection.searchMonth);
		this.$("#room").val(this.collection.searchRoom);
	},
	events: {
		'submit': function (ev) {
			ev.preventDefault();
			var year = this.$("#year").val();
			var month = this.$("#month").val();
			var room = this.$("#room").val();
			
			console.log("submit");
			console.log(year+","+month+","+room);
			
			this.trigger('search', year, month, room);
		}
	}
});

var PageRouter = Backbone.Router.extend({
	routes: {
		'search/(:year)+(:month)+(:room)':  'search'
	}
});

var searchResults = new Orders();

var resultView = new OrdersList({
	el: '#results',
	collection: searchResults,
	template: Handlebars.compile($('#order-list-template').html())
});


var router = new PageRouter();


var searchView = new SearchForm({
	el: '#search',
	collection: searchResults
});

var newYear=null, newMonth=null, newRoom=null;

router.on('route:search', function (year, month, room) {
	console.log("router.");
	searchResults.searchFor(year, month, room);
	newYear=year;
	newMonth=month;
	newRoom=room;
});

searchView.on('search', function (year, month, room) {
	console.log("searchView");
	router.navigate("search/"+encodeURIComponent(year)+"+"+encodeURIComponent(month)+"+"+encodeURIComponent(room), {trigger:true});
});
	
	Backbone.history.start();
	
$.fn.scrollSetPos = function(){
	console.log("scrollSetPos");
    if (localStorage) {
        var posReader = localStorage["posStorage"];
        $(window).scroll(function(e) {
            localStorage["posStorage"] = $(window).scrollTop();
			console.log("scrollSetPos: "+ localStorage["posStorage"]);
        });

        return true;
    }

    return false;
};

$.fn.scrollReadPos = function(){
	console.log("scrollReadPos");
    if (localStorage) {
        var posReader = localStorage["posStorage"];
        if (posReader) {
			console.log("scrollReadPos: "+posReader);
            $(window).scrollTop(posReader);
            localStorage.removeItem("posStorage");
        }
        return true;
    }

    return false;
};
    

$( document ).ready( function() {
	$(window).scrollSetPos();
	window.setInterval(function(){
			 console.log("reload");
			 searchResults.searchFor(newYear, newMonth, newRoom);
		 }, 5000);
});


function getTodayDate(){
	console.log("todayDate");
	var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1; //January is 0!
		var yyyy = today.getFullYear();
		if(dd<10){dd='0'+dd} if(mm<10){mm='0'+mm} today = yyyy+'-'+mm+'-'+dd;
		return today;
}

function getTodayYear(){
	console.log("todayYear");
	var today = new Date();
	return today.getFullYear();
}

function getTodayMonth(){
	console.log("todayMonth");
	var today = new Date();
	return today.getMonth()+1; //January is 0!
}

function sessionEmail(){
	var msg = "Welcome, "+this.getSessionEmail();
	document.getElementById('topMsg').innerHTML=msg;
}

function yearPicker(){
	console.log("yearPicker");
	for (i = new Date().getFullYear(); i > 1900; i--)
	{
		$('#year').append($('<option />').val(i).html(i));
	}
};

function todayDate(){
	document.getElementById('year').value=this.getTodayYear();
	//console.log(this.getTodayYear());
	document.getElementById('month').value=this.getTodayMonth();
	//console.log(this.getTodayMonth());
};


function checkLogin(){
	console.log("checkLogin");
	if (sessionStorage.getItem('status') != 'loggedIn'){
    //redirect to page
		window.location.replace("login.html");
	}
	else{
    //show validation message
		return false;
	}
};

function getSessionEmail(){
	return sessionStorage.getItem('email');
}

function logout(){
		$.ajax({
			url:'..'+logout_serverPath+'/logout.php',
			type:'post',
			success:function(){
				window.location.replace("login.html");
				sessionStorage.removeItem('status');
				sessionStorage.removeItem('email');
				sessionStorage.removeItem('fail');
			},
			error: function(){
				window.location.reload();
			}
		});
	};